//
//  APIEndpoint.swift
//  TLineExample
//
//  Created by Mateusz Danioł on 28/11/2021.
//

import Foundation

struct APIEndpoints {
    
    static func getItems() -> Endpoint<ItemsDTO> {
        return Endpoint(path: "getInventory", method: .get, queryParametersEncodable: nil)
    }
}
