//
//  ItemsResponseDTO+Mapping.swift
//  TLineExample
//
//  Created by Mateusz Danioł on 28/11/2021.
//

import Foundation

struct ItemsDTO: Decodable {
    var itemsList: [ItemDTO] = []
    
    init(from decoder: Decoder) throws {
        var container = try decoder.unkeyedContainer()
        while !container.isAtEnd {
            if let item = try? container.decode(ItemDTO.self) {
                itemsList.append(item)
            }
        }
    }
}


extension ItemsDTO {
    struct ItemDTO: Decodable {
        let id: String
        let type: String
        let color: String
        let available: Int
        let title: String
        let description: String
    }
}

extension ItemsDTO {
    
    func toDomain() -> ItemsPage {
        return .init(page: nil, totalPages: nil, items: itemsList.map { $0.toDomain()})
    }
}

extension ItemsDTO.ItemDTO {
    
    
    
    func toDomain() -> Item {
        return .init(id: Item.Identifier(id),
                     type: type,
                     color: color,
                     available: available,
                     title: title,
                     description: description)
    }
}
