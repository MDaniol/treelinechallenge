//
//  ItemsResponseStorage.swift
//  TLineExample
//
//  Created by Mateusz Danioł on 28/11/2021.
//

import Foundation

protocol ItemsResponseStorage {
    func getResponse(completion:  @escaping () -> Void ) // mocked for now
}
