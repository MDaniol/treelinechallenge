//
//  DefaultItemsRepository.swift
//  TLineExample
//
//  Created by Mateusz Danioł on 28/11/2021.
//

import Foundation

final class DefaultItemsRepository {
    private let dataTransferService: DataTransferService
    private let cache: ItemsResponseStorage?
    
    init(dataTransferService: DataTransferService, cache: ItemsResponseStorage?) {
        self.dataTransferService = dataTransferService
        self.cache = cache
    }
}

extension DefaultItemsRepository: ItemsRepository {
    
    public func fetchItemsList(cached: @escaping (ItemsPage) -> Void, completion: @escaping (Result<ItemsPage, Error>) -> Void) -> Cancellable? {
        
        let task = RepositoryTask()
        
        let endpoint = APIEndpoints.getItems()
        
        task.networkTask = self.dataTransferService.request(with: endpoint, completion: { result in
            switch result {
            case .success(let itemsResponseDTO):
                completion(.success(itemsResponseDTO.toDomain()))
            case .failure(let error):
                completion(.failure(error))
            }
        })
        
        return task
    }
}
