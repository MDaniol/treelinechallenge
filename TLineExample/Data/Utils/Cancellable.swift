//
//  Cancellable.swift
//  TLineExample
//
//  Created by Mateusz Danioł on 28/11/2021.
//

import Foundation

public protocol Cancellable {
    func cancel()
}
