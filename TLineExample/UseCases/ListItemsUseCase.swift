//
//  ListItemsUseCase.swift
//  TLineExample
//
//  Created by Mateusz Danioł on 28/11/2021.
//

import Foundation

protocol ListItemsUseCase {
    func execute(cached: @escaping (ItemsPage) -> Void,
                 completion: @escaping (Result<ItemsPage, Error>) -> Void) -> Cancellable?
}

final class DefaultListItemsUseCase: ListItemsUseCase {
    private let itemsRepository: ItemsRepository
    
    init(itemsRepository: ItemsRepository){
        self.itemsRepository = itemsRepository
    }
    
    func execute(cached: @escaping (ItemsPage) -> Void,
                 completion: @escaping (Result<ItemsPage, Error>) -> Void) -> Cancellable? {
        return itemsRepository.fetchItemsList(cached: cached, completion: { result in
            if case .success = result {
                completion(result)
            }
        })
    }
}


