//
//  AppFlowCoordinator.swift
//  TLineExample
//
//  Created by Mateusz Danioł on 28/11/2021.
//

import Foundation
import UIKit

final class AppFlowCoordinator {
    
    var navigationController: UINavigationController
    private let appDIContainer: AppDIContainer
    
    init(navigationController: UINavigationController,
         appDIContainer: AppDIContainer) {
        self.navigationController = navigationController
        self.appDIContainer = appDIContainer
    }
    
    func start() {
        let itemsSceneDIContainer = appDIContainer.makeItemsSceneDIContainer()
        let flow = itemsSceneDIContainer.makeItemsListFlowCoordinator(navigationController: navigationController)
        flow.start()
    }
    
}
