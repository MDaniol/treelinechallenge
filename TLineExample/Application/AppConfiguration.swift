//
//  AppConfiguration.swift
//  TLineExample
//
//  Created by Mateusz Danioł on 28/11/2021.
//

import Foundation

final class AppConfiguration {
    
    lazy var apiBaseURL: String = {
        guard let apiBaseURL = Bundle.main.object(forInfoDictionaryKey: "ApiURL") as? String else {
            fatalError("API URL not found")
        }
        return apiBaseURL
    }()
    
}
