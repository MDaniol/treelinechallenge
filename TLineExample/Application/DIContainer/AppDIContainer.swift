//
//  AppDIContainer.swift
//  TLineExample
//
//  Created by Mateusz Danioł on 28/11/2021.
//

import Foundation
import UIKit

final class AppDIContainer {
    
    lazy var appConfiguration = AppConfiguration()
    
    lazy var apiDataTransferService: DataTransferService = {
        let config = ApiDataNetworkConfig(baseURL: URL(string: appConfiguration.apiBaseURL)!,
                                          queryParameters: ["language": NSLocale.preferredLanguages.first ?? "en"])
        
        let apiDataNetwork = DefaultNetworkService(config: config)
        return DefaultDataTransferService(with: apiDataNetwork)
    }()
    
    // MARK: - DIContainers of scenes
    func makeItemsSceneDIContainer() -> ItemsSceneDIContainer {
        let dependencies = ItemsSceneDIContainer.Dependencies(apiDataTransferService: apiDataTransferService)
        return ItemsSceneDIContainer(dependencies: dependencies)
    }
}
