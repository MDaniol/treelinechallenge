//
//  MoviesSceneDIContainer.swift
//  TLineExample
//
//  Created by Mateusz Danioł on 28/11/2021.
//
import UIKit
import Foundation

final class ItemsSceneDIContainer {
    
    struct Dependencies {
        let apiDataTransferService: DataTransferService
    }
    
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func makeListItemsUseCase() -> ListItemsUseCase {
        return DefaultListItemsUseCase(itemsRepository: makeItemsRepository())
    }
    
    // MARK: - Repositories
    
    func makeItemsRepository() -> ItemsRepository {
        return DefaultItemsRepository(dataTransferService: dependencies.apiDataTransferService, cache: nil)
    }
    
    //MARK: - Items List
    
    func makeItemsListViewController(emailDelegate: EmailToBossDelegate) -> ItemsListViewController {
        return ItemsListViewController.create(with: makeItemsListViewModel(emailDelegate: emailDelegate))
    }
    
    func makeItemsListViewModel(emailDelegate: EmailToBossDelegate) -> ItemsListViewModel {
        return DefaultItemsListViewModel(listItemsUseCase: makeListItemsUseCase(), emailDelegate: emailDelegate)
    }
    
    func makeItemsListFlowCoordinator(navigationController: UINavigationController) -> ItemsListFlowCoordinator {
        return ItemsListFlowCoordinator(navigationController: navigationController, dependencies: self)
    }
}

extension ItemsSceneDIContainer: ItemsListFlowCoordinatorDependencies { }
