//
//  EmailComposer.swift
//  TLineExample
//
//  Created by Mateusz Danioł on 29/11/2021.
//

import Foundation

final class EmailComposer {
    static func composeEmailWithItems(items: [ItemListCellViewModel]) -> URL {
        let mailtoString = "mailto:bossman@bosscompany.com"
        let subjectString = "?subject=Sales Report"
        let bodyString = "&body=Todays sales report:\n\n\n"
        
        let salesItemsArray = items.map {$0.salesReportItem }
        let salesItemsString = salesItemsArray.joined()
        
        let overallString = (mailtoString + subjectString + bodyString + salesItemsString).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let mailtoUrl = URL(string: overallString!)!
        return mailtoUrl
    }
}
