//
//  ItemListCellViewModel.swift
//  TLineExample
//
//  Created by Mateusz Danioł on 28/11/2021.
//

import Foundation

struct ItemListCellViewModel: Equatable {
    let id: String
    let type: String
    let color: String
    let available: Int
    let title: String
    let description: String
    var soldItems: Int
    var salesReportItem: String {
        get {
            return "\(id), \(type), \(color), \(title), SOLD: \(soldItems)\n\n"
        }
    }
}

extension ItemListCellViewModel {
    
    init(item: Item) {
        self.id = item.id
        self.type = item.type
        self.color = item.color
        self.available = item.available
        self.title = item.title
        self.description = item.description
        soldItems = 0
    }
}
