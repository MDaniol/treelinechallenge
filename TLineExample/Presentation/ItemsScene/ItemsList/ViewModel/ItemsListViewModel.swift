//
//  ItemsListViewModel.swift
//  TLineExample
//
//  Created by Mateusz Danioł on 28/11/2021.
//

import UIKit
import Foundation

protocol ItemsListViewModelInput {
    func viewDidLoad()
    func didSelectItem(at index: Int)
    func updateList()
    func increaseSale(at index: Int)
    func decreaseSale(at index: Int)
    func sendEmailToBoss()
    
    var updateUI: (()->())? {get set}
}


protocol EmailToBossDelegate {
    func sendEmailToBoss(email: URL)
}

protocol ItemsListViewModelOutput {
    var items: [ItemListCellViewModel] {get}
}

protocol ItemsListViewModel: ItemsListViewModelInput, ItemsListViewModelOutput {}

final class DefaultItemsListViewModel: ItemsListViewModel {
    
    var updateUI: (()->())?
    
    private let listItemsUseCase: ListItemsUseCase
    private var itemsLoadTask: Cancellable? { willSet { itemsLoadTask?.cancel() } }
    private var emailDelegate: EmailToBossDelegate
    
    private var soldItems: [ItemListCellViewModel] {
        get {
            return items.filter { $0.soldItems > 0 }
        }
    }
    
    var items: [ItemListCellViewModel] = []
    
    init(listItemsUseCase: ListItemsUseCase,
         emailDelegate: EmailToBossDelegate) {
        self.listItemsUseCase = listItemsUseCase
        self.emailDelegate = emailDelegate
    }
    
    func updateList() {
        loadList()
    }
    
    func appendItems(_ pageWithItems: ItemsPage){
        items = pageWithItems.items.map(ItemListCellViewModel.init)
        updateUI?()
    }
    
    private func loadList() {
        itemsLoadTask = listItemsUseCase.execute(cached: appendItems, completion: { result in
            switch result {
            case .success(let parsedPageWithItems):
                self.appendItems(parsedPageWithItems)
            case .failure(let error):
                self.handle(error: error)
            }
        })
    }
    
    private func handle(error: Error) {
        print(error)
    }
    
}

extension DefaultItemsListViewModel {
    
    func viewDidLoad() {
        // implement in the future
    }
    
    func didSelectItem(at index: Int) {
        // implement in the future
    }
    
    func increaseSale(at index: Int) {
        if items[index].soldItems < items[index].available {
            items[index].soldItems += 1
            self.updateUI?()
        }
    }
    
    func decreaseSale(at index: Int) {
        if items[index].soldItems > 0 {
            items[index].soldItems -= 1
        }
        self.updateUI?()
    }
    
    func sendEmailToBoss() {
        let mailtoUrl = EmailComposer.composeEmailWithItems(items: soldItems)
        emailDelegate.sendEmailToBoss(email: mailtoUrl)
    }
}
