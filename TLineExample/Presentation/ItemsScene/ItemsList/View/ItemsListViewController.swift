//
//  ItemsListViewController.swift
//  TLineExample
//
//  Created by Mateusz Danioł on 28/11/2021.
//

import UIKit

class ItemsListViewController: UIViewController, StoryboardInstantiable {

    @IBOutlet weak var itemListTableView: UITableView!
    
    private var viewModel: ItemsListViewModel!
    
    static func create(with viewModel: ItemsListViewModel) -> ItemsListViewController {
        let view = ItemsListViewController.instantiateViewController()
        view.viewModel = viewModel
        return view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        itemListTableView.rowHeight = UITableView.automaticDimension
        itemListTableView.estimatedRowHeight = 200
        setupCallbacks()
        // Do any additional setup after loading the view.
    }
    
    private func setupCallbacks() {
        viewModel.updateUI = {
            self.itemListTableView.reloadData()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.updateList()
    }
    
    @IBAction func onSendEmailButtonTap(_ sender: Any) {
        viewModel.sendEmailToBoss()
    }

}

extension ItemsListViewController: UITableViewDelegate, UITableViewDataSource {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ItemListTableViewCell.reuseIdentifier,
                                                       for: indexPath) as? ItemListTableViewCell else {
            assertionFailure("Cannot dequeue reusable cell \(ItemListTableViewCell.self) with reuseIdentifier: \(ItemListTableViewCell.reuseIdentifier)")
            return UITableViewCell()
        }

        cell.fill(with: viewModel.items[indexPath.row])
        cell.decreaseSale = { [weak self] in
            if let index = self?.itemListTableView.indexPath(for: $0) {
                self?.viewModel.decreaseSale(at: index.row)
            }
        }
        
        cell .increaseSale = { [weak self] in
            if let index = self?.itemListTableView.indexPath(for: $0) {
                self?.viewModel.increaseSale(at: index.row)
            }
            
        }
        
        return cell
    }
}
