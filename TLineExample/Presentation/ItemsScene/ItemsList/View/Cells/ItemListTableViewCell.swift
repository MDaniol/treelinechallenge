//
//  TableViewCell.swift
//  TLineExample
//
//  Created by Mateusz Danioł on 28/11/2021.
//

import UIKit

class ItemListTableViewCell: UITableViewCell {

    var increaseSale: ((ItemListTableViewCell) -> ())?
    var decreaseSale: ((ItemListTableViewCell) -> ())?
    
    static let reuseIdentifier = String(describing: ItemListTableViewCell.self)
    
    @IBOutlet weak var itemTitleLabel: UILabel!
    @IBOutlet weak var itemOverallCountLabel: UILabel!
    @IBOutlet weak var itemSoldNumberLabel: UILabel!
    @IBOutlet weak var sellLessButton: UIButton!
    @IBOutlet weak var sellMoreButton: UIButton!
    
    private var viewModel: ItemListCellViewModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fill(with viewModel: ItemListCellViewModel) {
        self.viewModel = viewModel
        itemTitleLabel.text =  viewModel.type + ", " + viewModel.color + ", " + viewModel.title
        itemOverallCountLabel.text = "Available: \(viewModel.available)"
        itemSoldNumberLabel.text = "Sold: \(viewModel.soldItems)"
    }
    
    @IBAction func onIncreaseSaleButtonTap(_ sender: Any) {
        increaseSale?(self)
    }
    
    @IBAction func onDecreaseSaleButtonTap(_ sender: Any) {
        decreaseSale?(self)
    }
    
    

}
