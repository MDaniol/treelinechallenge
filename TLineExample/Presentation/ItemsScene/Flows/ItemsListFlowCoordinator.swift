//
//  ItemsListFlowCoordinator.swift
//  TLineExample
//
//  Created by Mateusz Danioł on 28/11/2021.
//

import Foundation
import UIKit

protocol ItemsListFlowCoordinatorDependencies {
    func makeItemsListViewController(emailDelegate: EmailToBossDelegate) -> ItemsListViewController
}

final class ItemsListFlowCoordinator {
    private weak var navigationController: UINavigationController?
    private let dependencies: ItemsListFlowCoordinatorDependencies
    
    private weak var itemsListVC: ItemsListViewController?
    
    
    init(navigationController: UINavigationController,
         dependencies: ItemsListFlowCoordinatorDependencies) {
        self.navigationController = navigationController
        self.dependencies = dependencies
    }
    
    func start() {
        let vc = dependencies.makeItemsListViewController(emailDelegate: self)
        navigationController?.pushViewController(vc, animated: false)
        
        itemsListVC = vc
    }
}

extension ItemsListFlowCoordinator: EmailToBossDelegate {
    func sendEmailToBoss(email: URL) {
        if UIApplication.shared.canOpenURL(email) {
                UIApplication.shared.open(email, options: [:])
        }
    }
}
