//
//  ItemsRepository.swift
//  TLineExample
//
//  Created by Mateusz Danioł on 28/11/2021.
//

import Foundation

protocol ItemsRepository {
    @discardableResult
    func fetchItemsList(cached: @escaping (ItemsPage) -> Void,
                    completion: @escaping (Result<ItemsPage, Error>) -> Void) -> Cancellable?
    
}
