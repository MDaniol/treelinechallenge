//
//  Item.swift
//  TLineExample
//
//  Created by Mateusz Danioł on 28/11/2021.
//

import Foundation

struct Item: Equatable, Identifiable {
    typealias Identifier = String
    
    let id: Identifier
    let type: String
    let color: String
    let available: Int
    let title: String
    let description: String
}

struct ItemsPage: Equatable {
    // prepared for pagination
    let page: Int?
    let totalPages: Int?
    
    let items: [Item]
}
