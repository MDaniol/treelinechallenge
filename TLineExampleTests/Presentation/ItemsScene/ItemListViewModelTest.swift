//
//  ItemListViewModelTest.swift
//  TLineExampleTests
//
//  Created by Mateusz Danioł on 29/11/2021.
//
@testable import TLineExample
import XCTest

class ItemListViewModelTest: XCTestCase {

    let mockedItems: [Item] = [
        Item(id: "1", type: "Shirt", color: "black", available: 0, title: "Spiderman", description: "Nice Shirt"),
        Item(id: "2", type: "Shirt", color: "white", available: 10, title: "Batman", description: "Very nice Shirt"),
        Item(id: "3", type: "Shirt", color: "green", available: 1, title: "Captain America", description: "Nice Shirt"),
        Item(id: "4", type: "Shirt", color: "yellow", available: 12, title: "Flash", description: "Nice Shirt")
    ]
    
    class ListItemsUseCaseMock: ListItemsUseCase {
        var expectation: XCTestExpectation?
        var error: Error?
        var items = ItemsPage(page: nil, totalPages: nil, items: [])
        
        func execute(cached: @escaping (ItemsPage) -> Void,
                     completion: @escaping (Result<ItemsPage, Error>) -> Void) -> Cancellable? {
            if let error = error {
                completion(.failure(error))
            } else {
                completion(.success(items))
            }
            expectation?.fulfill()
            return nil
        }
    }
    
    class MockedEmailToBossDelegate: EmailToBossDelegate {
        var emailURL: URL?
        
        func sendEmailToBoss(email: URL) {
            emailURL = email
        }
    }
    
    func test_fetchingMoviesFromAPI() {
        let listItemsUseCaseMock = ListItemsUseCaseMock()
        listItemsUseCaseMock.expectation = self.expectation(description: "contains 4 items")
        listItemsUseCaseMock.items = ItemsPage(page: nil, totalPages: nil, items: mockedItems)
        let viewModel = DefaultItemsListViewModel(listItemsUseCase: listItemsUseCaseMock, emailDelegate: MockedEmailToBossDelegate())
        // when
        viewModel.updateList()
        
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertEqual(viewModel.items.count, 4)
        
        // Check if the data is correct
        XCTAssertEqual(viewModel.items[0].id, "1")
        XCTAssertEqual(viewModel.items[0].color, "black")
        XCTAssertEqual(viewModel.items[0].available, 0)
        XCTAssertEqual(viewModel.items[0].title, "Spiderman")
        
        XCTAssertEqual(viewModel.items[1].id, "2")
        XCTAssertEqual(viewModel.items[1].color, "white")
        XCTAssertEqual(viewModel.items[1].available, 10)
        XCTAssertEqual(viewModel.items[1].title, "Batman")
        
        XCTAssertEqual(viewModel.items[2].id, "3")
        XCTAssertEqual(viewModel.items[2].color, "green")
        XCTAssertEqual(viewModel.items[2].available, 1)
        XCTAssertEqual(viewModel.items[2].title, "Captain America")
        
        XCTAssertEqual(viewModel.items[3].id, "4")
        XCTAssertEqual(viewModel.items[3].color, "yellow")
        XCTAssertEqual(viewModel.items[3].available, 12)
        XCTAssertEqual(viewModel.items[3].title, "Flash")
    }
    
    func test_whenPushingAddSoldItemButton() {
        let listItemsUseCaseMock = ListItemsUseCaseMock()
        listItemsUseCaseMock.expectation = self.expectation(description: "contains 4 items")
        listItemsUseCaseMock.items = ItemsPage(page: nil, totalPages: nil, items: mockedItems)
        let viewModel = DefaultItemsListViewModel(listItemsUseCase: listItemsUseCaseMock, emailDelegate: MockedEmailToBossDelegate())
        // when
        viewModel.updateList()
        waitForExpectations(timeout: 5, handler: nil)
        viewModel.increaseSale(at: 2)
        XCTAssertEqual(viewModel.items[2].soldItems, 1)
        // despite pushing the button to increase sale, we shouldn't be able to do this as available items is 1
        viewModel.increaseSale(at: 2)
        XCTAssertEqual(viewModel.items[2].soldItems, 1)
        
    }
    
    func test_whenPushingSubstractSoldItemButton() {
        let listItemsUseCaseMock = ListItemsUseCaseMock()
        listItemsUseCaseMock.expectation = self.expectation(description: "contains 4 items")
        listItemsUseCaseMock.items = ItemsPage(page: nil, totalPages: nil, items: mockedItems)
        let viewModel = DefaultItemsListViewModel(listItemsUseCase: listItemsUseCaseMock, emailDelegate: MockedEmailToBossDelegate())
        // when
        viewModel.updateList()
        waitForExpectations(timeout: 5, handler: nil)
        viewModel.items[2].soldItems = 1
        viewModel.decreaseSale(at: 2)
        XCTAssertEqual(viewModel.items[2].soldItems, 0)
        // despite pushing the button to decrease sale, we shouldn't be able to do this as available items is 1
        viewModel.decreaseSale(at: 2)
        XCTAssertEqual(viewModel.items[2].soldItems, 0)
    }
    
    func test_whenSendingEmailWithSoldItems() {
        let listItemsUseCaseMock = ListItemsUseCaseMock()
        let emailDelegate = MockedEmailToBossDelegate()
        listItemsUseCaseMock.expectation = self.expectation(description: "contains 4 items")
        listItemsUseCaseMock.items = ItemsPage(page: nil, totalPages: nil, items: mockedItems)
        let viewModel = DefaultItemsListViewModel(listItemsUseCase: listItemsUseCaseMock, emailDelegate: emailDelegate)
        // when
        viewModel.updateList()
    
        viewModel.increaseSale(at: 2)
    
        viewModel.sendEmailToBoss()
        let stringUrl = emailDelegate.emailURL!.absoluteString
        
        XCTAssertEqual(stringUrl, "mailto:bossman@bosscompany.com?subject=Sales%20Report&body=Todays%20sales%20report:%0A%0A%0A3,%20Shirt,%20green,%20Captain%20America,%20SOLD:%201%0A%0A")
        
        waitForExpectations(timeout: 5, handler: nil)
    }
}
